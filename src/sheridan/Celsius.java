package sheridan;

public class Celsius {

	public static void main(String[] args) {
		
		System.out.println("198 in Fahrenheit is "+convertFromFahrenheit(198)+" in Celsius.");
		
	}
	
	public static int convertFromFahrenheit(int fDegree) {
		
		int cDegree=Math.round(((fDegree-32)*5)/9);
		
		return cDegree;
	}

}
